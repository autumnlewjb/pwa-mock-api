import {
  Body,
  Controller,
  Get,
  ParseIntPipe,
  Post,
  Query,
} from '@nestjs/common';
import { CommentsService } from './comments.service';
import { CreateCommentDto } from './CreateComment.dto';

@Controller('comments')
export class CommentsController {
  constructor(private readonly commentsService: CommentsService) {}

  @Get()
  async getAllComments() {
    return this.commentsService.getAllComments();
  }
  @Get('count')
  async getNewCommentCount(@Query('from', ParseIntPipe) startsFrom: Date) {
    return this.commentsService.countNewComment(startsFrom);
  }

  @Post()
  async writeNewComment(@Body() comment: CreateCommentDto) {
    return this.commentsService.writeComment(comment);
  }
}
