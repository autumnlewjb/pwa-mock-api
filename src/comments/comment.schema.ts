import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

export type CommentDocument = Comment & Document;

@Schema({
  timestamps: true,
})
export class Comment {
  @Prop({ required: true })
  text: string;

  @Prop()
  writtenBy: string;
}

export const CommentSchema = SchemaFactory.createForClass(Comment);
