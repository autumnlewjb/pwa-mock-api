export class CreateCommentDto {
  text: string;
  writtenBy: string;
}
