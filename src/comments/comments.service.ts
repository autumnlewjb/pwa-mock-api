import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CommentDocument, Comment } from './comment.schema';
import { Model } from 'mongoose';
import { CreateCommentDto } from './CreateComment.dto';

@Injectable()
export class CommentsService {
  constructor(
    @InjectModel(Comment.name) private commentModel: Model<CommentDocument>,
  ) {}

  async countNewComment(startsFrom: Date) {
    return this.commentModel.find({ createdAt: { $gt: startsFrom } }).count();
  }

  async writeComment(comment: CreateCommentDto): Promise<Comment> {
    const createdComment = new this.commentModel(comment);
    return createdComment.save();
  }

  async getAllComments(): Promise<Comment[]> {
    return this.commentModel.find({});
  }
}
