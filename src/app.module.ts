import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CommentSchema } from './comments/comment.schema';
import { CommentsModule } from './comments/comments.module';

@Module({
  imports: [
    CommentsModule,
    MongooseModule.forRoot(
      'mongodb://admin:secret@localhost:27017/comment-db?authSource=admin',
    ),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
