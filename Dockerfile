FROM node:16-alpine AS builder
WORKDIR /app
ENV MONGODB_CONN_URL=mongodb
COPY . .
RUN npm i
RUN npm run build

FROM node:16-alpine
WORKDIR /app/dist
COPY package*.json .
RUN npm i
COPY --from=builder /app/dist /app/node_modules ./
CMD node main
